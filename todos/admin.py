from django.contrib import admin
from todos.models import TodoList, TodoItem


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
        'created_on'
    ]


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'task',
        'is_completed',
        'list_name'
    ]
