from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoItem, TodoList
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        'todo_list': todo_list
    }
    return render(request, "todos/todos.html", context)


def todo_list_detail(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {"list": list}
    return render(request, 'todos/detail.html', context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
        context = {
            'form': form
        }
        return render(request, 'todos/create.html', context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=list)
        context = {
            "list": list,
            "form": form
        }
        return render(request, 'todos/edit.html', context)


def todo_list_delete(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            list = todo_item.list
            # redirect to the list detail page
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoItemForm()
        context = {
            'form': form
        }
        return render(request, 'todos/item_create.html', context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            todo_item = form.save()
            list = todo_item.list
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoItemForm(instance=item)
        context = {
            "form": form
        }
        return render(request, 'todos/item_edit.html', context)
